package ru.t1.godyna.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.godyna.tm.enumerated.Role;
import ru.t1.godyna.tm.model.User;
import ru.t1.godyna.tm.repository.model.UserRepository;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

public interface IUserService {

    UserRepository getRepository(@NotNull EntityManager entityManager);

    @NotNull
    User create(@Nullable String login, @Nullable String password);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable Role role);

    boolean existsById(@Nullable String id);

    @NotNull
    List<User> findAll();

    @Nullable
    User findOneById(@Nullable String id);

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User findByEmail(@Nullable String email);

    boolean isLoginExist(@Nullable String login);

    boolean isEmailExist(@Nullable String email);

    @NotNull
    User remove(@Nullable User user);

    @NotNull
    User removeById(@Nullable String id);

    @NotNull
    User removeByLogin(@Nullable String login);

    @NotNull
    User removeByEmail(@Nullable String email);

    @NotNull
    Collection<User> set(@NotNull Collection<User> users);

    @NotNull
    User setPassword(@Nullable String id, @Nullable String password);

    @NotNull
    User updateUser(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    );

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

}
