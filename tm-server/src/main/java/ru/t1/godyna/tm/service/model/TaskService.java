package ru.t1.godyna.tm.service.model;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.t1.godyna.tm.api.repository.model.ITaskRepository;
import ru.t1.godyna.tm.api.service.ITaskService;
import ru.t1.godyna.tm.comparator.NameComparator;
import ru.t1.godyna.tm.enumerated.Sort;
import ru.t1.godyna.tm.enumerated.Status;
import ru.t1.godyna.tm.exception.entity.TaskNotFoundException;
import ru.t1.godyna.tm.exception.field.*;
import ru.t1.godyna.tm.model.Task;
import ru.t1.godyna.tm.model.User;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Service
@AllArgsConstructor
public final class TaskService implements ITaskService {

    @NotNull
    @Autowired
    private ApplicationContext context;

    @NotNull
    private EntityManager getEntityManager() {
        return context.getBean(EntityManager.class);
    }

    @NotNull
    @Override
    public ITaskRepository getRepository(@NotNull final EntityManager entityManager) {
        return context.getBean(ITaskRepository.class);
    }

    @NotNull
    @Override
    public Task add(@Nullable Task model) {
        if (model == null) throw new TaskNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository taskRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            taskRepository.add(model);
            entityManager.getTransaction().commit();
            return model;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Task add(@Nullable String userId, @Nullable Task model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new TaskNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository taskRepository = getRepository(entityManager);
        try {
            model.setUser(entityManager.find(User.class, userId));
            entityManager.getTransaction().begin();
            taskRepository.add(model);
            entityManager.getTransaction().commit();
            return model;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Collection<Task> add(@NotNull Collection<Task> models) {
        if (models.isEmpty()) throw new TaskNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository taskRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            for (@NotNull Task task : models) {
                taskRepository.add(task);
            }
            entityManager.getTransaction().commit();
            return models;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Task changeTaskStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return update(task);
    }

    @Override
    public void clear() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository taskRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            taskRepository.clearAll();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository taskRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            taskRepository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Task create(@Nullable String userId, @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull Task task = new Task();
        @NotNull final EntityManager entityManager = getEntityManager();
        task.setUser(entityManager.find(User.class, userId));
        task.setName(name);
        return add(task);
    }

    @NotNull
    @Override
    public Task create(@Nullable String userId, @Nullable String name, @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull Task task = new Task();
        @NotNull final EntityManager entityManager = getEntityManager();
        task.setUser(entityManager.find(User.class, userId));
        task.setName(name);
        task.setDescription(description);
        return add(task);
    }

    @Override
    public boolean existsById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository taskRepository = getRepository(entityManager);
        try {
            return taskRepository.existsById(id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existsById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository taskRepository = getRepository(entityManager);
        try {
            return taskRepository.existsByIdUserId(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public List<Task> findAll() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository taskRepository = getRepository(entityManager);
        try {
            return taskRepository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public List<Task> findAll(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository taskRepository = getRepository(entityManager);
        try {
            return taskRepository.findAllUserId(userId);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public List<Task> findAll(@Nullable String userId, @Nullable Sort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository taskRepository = getRepository(entityManager);
        try {
            if(sort == null) return taskRepository.findAllUserId(userId, NameComparator.INSTANCE);
            return taskRepository.findAllUserId(userId, sort);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public List<Task> findAll(@Nullable Comparator<Task> comparator) {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository taskRepository = getRepository(entityManager);
        try {
            if (comparator == null) return taskRepository.findAll(NameComparator.INSTANCE);
            return taskRepository.findAll(comparator);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public List<Task> findAll(@Nullable String userId, @Nullable Comparator<Task> comparator) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository taskRepository = getRepository(entityManager);
        try {
            if (comparator == null) return taskRepository.findAllUserId(userId, NameComparator.INSTANCE);
            return taskRepository.findAllUserId(userId, comparator);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public List<Task> findAllByProjectId(@Nullable String userId, @Nullable String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository taskRepository = getRepository(entityManager);
        try {
            return taskRepository.findAllByProjectId(userId, projectId);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public Task findOneById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository taskRepository = getRepository(entityManager);
        try {
            return taskRepository.findOneById(id);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public Task findOneById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository taskRepository = getRepository(entityManager);
        try {
            return taskRepository.findOneByIdUserId(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public long getSize() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository taskRepository = getRepository(entityManager);
        try {
            return taskRepository.getSize();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public long getSize(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository taskRepository = getRepository(entityManager);
        try {
            return taskRepository.getSizeUserId(userId);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Task remove(@Nullable Task model) {
        if (model == null) throw new TaskNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository taskRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            taskRepository.remove(model);
            entityManager.getTransaction().commit();
            return model;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Task remove(@Nullable String userId, @Nullable Task model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new TaskNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository taskRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            taskRepository.removeByIdUserId(userId, model.getId());
            entityManager.getTransaction().commit();
            return model;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeAll(@Nullable Collection<Task> collection) {
        if (collection == null) throw new TaskNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository taskRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            for (@NotNull Task task : collection) {
                taskRepository.remove(task);
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Task removeById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository taskRepository = getRepository(entityManager);
        try {
            @Nullable Task task = findOneById(id);
            entityManager.getTransaction().begin();
            taskRepository.removeById(id);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Task removeById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository taskRepository = getRepository(entityManager);
        try {
            @Nullable Task task = findOneById(id);
            entityManager.getTransaction().begin();
            taskRepository.removeById(id);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Collection<Task> set(@NotNull Collection<Task> models) {
        if (models.isEmpty()) throw new TaskNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository taskRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            clear();
            add(models);
            entityManager.getTransaction().commit();
            return models;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Task update(@NotNull Task model) {
        if (model == null) throw new TaskNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepository taskRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            taskRepository.update(model);
            entityManager.getTransaction().commit();
            return model;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Task updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new IndexIncorrectException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        task.setName(name);
        task.setDescription(description);
        task.setUser(entityManager.find(User.class, userId));
        @NotNull final ITaskRepository taskRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
