package ru.t1.godyna.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.godyna.tm.enumerated.Role;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Cacheable
@NoArgsConstructor
@Table(name = "tm_session")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class Session extends AbstractModel {

    @Nullable
    private String userId;

    @NotNull
    @Column(name = "created", nullable = false)
    private Date date = new Date();

    @Nullable
    @Column(name = "role", nullable = true)
    @Enumerated(EnumType.STRING)
    private Role role = null;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

}
