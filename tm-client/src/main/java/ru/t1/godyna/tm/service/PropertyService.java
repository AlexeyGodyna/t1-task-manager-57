package ru.t1.godyna.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.godyna.tm.api.service.IPropertyService;

import java.util.Properties;

@Service
@PropertySource("classpath:application.properties")
public final class PropertyService implements IPropertyService {

    @Getter
    @NotNull
    @Value("#{environment['server.host']}")
    private String host;

    @Getter
    @NotNull
    @Value("#{environment['server.port']}")
    private String port;

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    private static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    private static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    @Override
    public String getApplicationVersion() {
        return Manifests.read(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return Manifests.read(AUTHOR_EMAIL_KEY);
    }

    @NotNull
    @Override
    public String getAuthorName() {
        return Manifests.read(AUTHOR_NAME_KEY);
    }

    @NotNull
    @Override
    public String getHost() {
        return host;
    }

    @NotNull
    @Override
    public String getPort() {
        return port;
    }

}
